# template for "Stopwatch: The Game"
# http://www.codeskulptor.org/#user19_EgKf1ggn4z_2.py
import simplegui


# define global variables
timer1 = 0
counter1 = 0
counter2 = 0
flag = False


# define helper function format that converts time
# in tenths of seconds into formatted string A:BC.D
def format(t):
    if (t%600)//10<10:
        temp = "0"
    else:
        temp = ""
    return (str(t // 600) + ":" +
           temp + str((t % 600) // 10) +
           "." + str((t % 600) % 10))
 
    
# define event handlers for buttons; "Start", "Stop", "Reset"
def start():
    timer.start()
    global flag
    flag = True
   
def stop():
    timer.stop()
    global flag
    if flag:
        flag = False
    else:
        return
    global counter1, counter2
    counter1 += 1
    if (timer1 % 600) % 10 == 0:
        counter2 += 1

def reset():
    timer.stop()
    global timer1, counter1, counter2
    timer1 = 0
    counter1 = 0
    counter2 = 0

# define event handler for timer with 0.1 sec interval
def timer_handler():
    global timer1
    timer1 += 1 

# define draw handler
def draw(canvas):
    canvas.draw_text(format(timer1), [110, 110],
                     38, "White")
    canvas.draw_text(str(counter2) + "/" + str(counter1),
                     [240, 20], 24, "red")
    
# create frame
frame = simplegui.create_frame("Stopwatch", 300, 200)


# Start the frame animation
timer = simplegui.create_timer(100, timer_handler)

# register event handlers
frame.add_button("Start", start, 100)
frame.add_button("Stop", stop, 100)
frame.add_button("Reset", reset, 100)

#frame.set_draw_handler(draw_handler)
frame.set_draw_handler(draw)

# start frame
frame.start()

# Please remember to review the grading rubric
