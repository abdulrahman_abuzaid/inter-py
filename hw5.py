# Mini-project #6 - Blackjack
# http://www.codeskulptor.org/#user19_HJoaErf1gc_6.py

import simplegui
import random

# load card sprite - 949x392 - source: jfitz.com
CARD_SIZE = (73, 98)
CARD_CENTER = (36.5, 49)
card_images = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/cards.jfitz.png")

CARD_BACK_SIZE = (71, 96)
CARD_BACK_CENTER = (35.5, 48)
card_back = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/card_back.png")    

# initialize some useful global variables
in_play = False
outcome = ""
score = 0
busted = False
# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7',
         '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5,
          '6':6, '7':7, '8':8, '9':9, 'T':10,
          'J':10, 'Q':10, 'K':10}


# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank), 
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        canvas.draw_image(card_images, card_loc, CARD_SIZE,
                          [pos[0] + CARD_CENTER[0],
                           pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
# define hand class
class Hand:
    def __init__(self):
        # create Hand object
        self.hand = []
        
    def __str__(self):
        # return a string representation of a hand
        string = ""
        for i in self.hand:
            string += (str(i) + " ")
        return "Hand contains " + string

    def add_card(self, card):
        # add a card object to a hand
        self.hand.append(card)
        

    def get_value(self):
        # count aces as 1, if the hand has an ace, then add 10 to hand value if it doesn't bust
        # compute the value of the hand, see Blackjack video
        self.value = 0
        self.flag = False
        for i in self.hand:
            if i.get_rank() == 'A':
                self.flag = True
            self.value += VALUES.get(i.get_rank())
        if self.flag and self.value<=11:
            self.value += 10
        return self.value
    def draw(self, canvas, pos):
        # draw a hand on the canvas, use the draw method for cards
        j = 0
        for i in self.hand:
            i.draw(canvas,
                   [pos[0] + j * (CARD_SIZE[0] + 10),
                    pos[1]])
            j += 1
            
# define deck class 
class Deck:
    def __init__(self):
        # create a Deck object
        self.deck = []
        for i in range(4):
            for j in range(13):
                self.deck.append(Card(SUITS[i], RANKS[j]))
        #random.shuffle(self.deck)
    def shuffle(self):
        # add cards back to deck and shuffle
        # use random.shuffle() to shuffle the deck
        self.__init__()
        random.shuffle(self.deck)

    def deal_card(self):
        # deal a card object from the deck
        return self.deck.pop()
    
    def __str__(self):
        # return a string representing the deck
        string = "Deck contains "
        for i in self.deck:
            string += (str(i) + " ")
        return string

#define event handlers for buttons
def deal():
    global msg, busted, outcome, in_play
    global deck, player, dealer
    if in_play:
        outcome -= 1
    in_play = True
    busted = False
    msg = "Hit or stand?"
    player = Hand()
    dealer = Hand()
    deck = Deck()
    deck.shuffle()
    player.add_card(deck.deal_card())
    player.add_card(deck.deal_card())
    dealer.add_card(deck.deal_card())
    dealer.add_card(deck.deal_card())
    
def hit():
    global msg, busted, in_play, outcome
    busted = False
    if in_play and player.get_value() <= 21:
        player.add_card(deck.deal_card())
    
    if in_play and player.get_value() > 21:
        msg = "Player busted"
        busted = True
        in_play = False
        outcome -= 1
       
def stand():
    global msg, in_play, outcome
    if busted:
        msg = "Player busted"
    elif in_play:
        while (dealer.get_value()) < 17:
            dealer.add_card(deck.deal_card())
    if in_play and dealer.get_value() > 21:
        msg = "Dealer busted"
        in_play = False
        outcome += 1
        
    if in_play:      
        if dealer.get_value() >= player.get_value():
            msg = "Dealer wins"
            in_play = False
            outcome -= 1
        else:
            msg = "Player wins"
            in_play = False
            outcome += 1

   
    # if hand is in play, repeatedly hit dealer until his hand has value 17 or more

    # assign a message to outcome, update in_play and score

# draw handler    
def draw(canvas):
    # test to make sure that card.draw works, replace with your code below      
    player.draw(canvas, [100, 350])
    dealer.draw(canvas, [100, 150])
    canvas.draw_text("Result = "+str(outcome),
                     [215, 110], 36, "Black")
    canvas.draw_text(msg, [300, 315], 48, "Black")
    canvas.draw_text("Blackjack", [200, 50], 48, "Black")
    if in_play:
        canvas.draw_image(card_back, CARD_BACK_CENTER,
                          CARD_BACK_SIZE,
                          [100 + CARD_BACK_CENTER[0],
                           150 + CARD_BACK_CENTER[1]],
                          CARD_SIZE)
    else:
        canvas.draw_text("New deal?", [300, 515],
                         48, "Black")
        


# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)


# get things rolling
msg = "Hit or stand?"
in_play = False
outcome = 0
player = Hand()
dealer = Hand()
deal()
frame.start()




# remember to review the gradic rubric