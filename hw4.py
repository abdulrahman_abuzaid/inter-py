# implementation of card game - Memory
# http://www.codeskulptor.org/#user19_CWGaBNseKp_5.py

import simplegui
import random

counter = 0

# helper function to initialize globals
def init():
    global numbers_list, exposed, state
    global first, second, counter
    numbers_list = range(8)
    numbers_list.extend(range(8))
    random.shuffle(numbers_list)
    exposed = [False]*16
    state = 0
    counter = 0
    label.set_text("Moves = " + str(counter))

     
# define event handlers
def mouseclick(pos):
    global exposed, state, first, second, counter
    if not exposed[pos[0] // 50]:
        if state == 0:
            state = 1
            
            first = pos[0] // 50
        elif state == 1:
            state = 2
            second = pos[0] // 50
            counter += 1
        else:
            if numbers_list[first] != numbers_list[second]:
                exposed[first] = False
                exposed[second] = False
            first = pos[0] // 50
            state = 1
            
        # add game state logic here
        exposed[pos[0] // 50] = True
        label.set_text("Moves = " + str(counter))
        
        
        
# cards are logically 50x100 pixels in size    
def draw(canvas):
    global numbers_list, exposed
    for i in range(16):
        if exposed[i]:
            canvas.draw_text(str(numbers_list[i]),
                             [15 + 50 * i, 60], 48, "White")
        else:
            canvas.draw_polygon([[0 + 50 * i, 0],
                                 [0 + 50 * i, 100],
                                 [50 + 50 * i, 100],
                                 [50 + 50 * i, 0]],
                                1, "Red", "Green")

            
# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Restart", init)
label = frame.add_label("Moves = 0")


# initialize global variables
init()


# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)


# get things rolling
frame.start()

# Always remember to review the grading rubric