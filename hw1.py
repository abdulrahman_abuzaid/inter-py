# Rock-paper-scissors-lizard-Spock template
# http://www.codeskulptor.org/#user19_OjrYnxIrpX_4.py


# The key idea of this program is to equate the strings
# "rock", "paper", "scissors", "lizard", "Spock" to numbers
# as follows:
#
# 0 - rock
# 1 - Spock
# 2 - paper
# 3 - lizard
# 4 - scissors

# helper functions
import random
def number_to_name(number):
    if number == 0:
        name = "rock"
    elif number == 1:
        name = "Spock"
    elif number ==  2:
        name = "paper"
    elif number == 3:
        name = "lizard"
    else:
        name = "scissors"
    return name
    
def name_to_number(name):
    # fill in your code below
    # convert name to number using if/elif/else
    # don't forget to return the result!
    if name == "rock":
        number = 0
    elif name == "Spock":
        number = 1
    elif name ==  "paper":
        number = 2
    elif name == "lizard":
        number = 3
    else:
        number = 4
    return number    


def rpsls(name): 
    # convert name to player_number using name_to_number
    player_number = name_to_number(name)
    
    # compute random guess for comp_number using random.randrange()
    comp_number = random.randrange(5)
    
    # compute difference of player_number and comp_number modulo five
    difference = comp_number - player_number
    
    # use if/elif/else to determine winner
    if difference % 5 == 0:
        result = "Player and computer Tie!"
    elif difference % 5 == 1:
        result = "Computer wins!"
    elif difference % 5 == 2:
        result = "Computer wins!"
    elif difference % 5 == 3:
        result = "Player wins!"
    elif difference % 5 == 4:
        result = "Player wins!"
        
    # convert comp_number to name using number_to_name
    comp_name = number_to_name(comp_number)
    
    # print results
    print "Player chooses " + name
    print "Computer chooses " + comp_name
    print result
    print ""

# test your code
rpsls("rock")
rpsls("Spock")
rpsls("paper")
rpsls("lizard")
rpsls("scissors")

# always remember to check your completed program against the grading rubric


